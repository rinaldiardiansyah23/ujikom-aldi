<!DOCTYPE html>
<html lang="en">

<head>
	<title>SB Admin 2 - Bootstrap Admin Theme</title>

	<?php include 'links.php'; ?>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

    	<?php include 'header.php'; ?>

    	<div id="wrapper">

    		<!-- Navigation -->
    		
    		<div id="page-wrapper">
    			<div class="row">
    				<div class="col-lg-12">
    					<h1 class="page-header">Dashboard</h1>
    				</div>
    				<!-- /.col-lg-12 -->
    			</div>
    			<!-- /.row -->
    			<h3>EDIT DATA BARANG</h3>
    			
    			<?php
    			include 'koneksi.php';
    			$id = $_GET['id'];
    			$data = mysqli_query($koneksi,"select * from inventaris where id_inventaris='$id'");
    			while($d = mysqli_fetch_array($data)){
    				?>
    				<div class="panel-body">
    					<div class="row">
    						<div class="col-lg-12">
    							<form method="post" action="update_barang.php">
    								<table>
    									<tr>			
    										<td>NAMA</td>
    										<td>
    											<input type="hidden" name="id" value="<?php echo $d['id_inventaris']; ?>">
    											<input type="text" name="nama" value="<?php echo $d['nama']; ?>">
    										</td>
    									</tr>
    									<tr>
    										<td>KONDISI</td>
    										<td><input type="text" name="kondisi" value="<?php echo $d['kondisi']; ?>"></td>
    									</tr>
    									<tr>
    										<td>KETERANGAN</td>
    										<td><input type="text" name="keterangan" value="<?php echo $d['keterangan']; ?>"></td>
    									</tr>
    									<tr>
    										<td>JUMLAH</td>
    										<td><input type="text" name="jumlah" autocomplete="off" value="<?php echo $d['jumlah']; ?>"></td>
    									</tr>
    									<tr>
    										<td>JENIS</td>
    										<td>
    											<select name="nama_jenis" class="form-control">
    												<option hidden>Pilih jenis</option>
    												<?php
    												$query=mysqli_query($koneksi,"SELECT * FROM jenis");
    												while ($show=mysqli_fetch_array($query)) {?>
    												<option value="<?=$show['id_jenis'];?>" <?=($d['id_jenis']==$show['id_jenis'])?'selected':''?>><?=$show['nama_jenis'];?>
    												</option>
    												<?php
    											}
    											?>
    										</select>
    									</td>
    								</tr>
    								<tr>
    									<td>TANGGAL</td>
    									<td><input type="date" name="tanggal_register" value="<?php echo $d['tanggal_register']; ?>"></td>	
    								</tr>
    								<tr>
    									<td>RUANG</td>
    									<td>
    										<select name="nama_ruang" class="form-control">
    											<option hidden>Pilih ruang</option>
    											<?php
    											$query=mysqli_query($koneksi,"SELECT * FROM ruang");
    											while ($show=mysqli_fetch_array($query)) {?>
    											<option value="<?=$show['id_ruang'];?>" <?=($d['id_ruang']==$show['id_ruang'])?'selected':''?>><?=$show['nama_ruang'];?>
    											</option>
    											<?php
    										}
    										?>
    									</select>
    								</td>
    							</tr>
    							<tr>
    								<td>KODE</td>
    								<td><input type="text" autocomplete="off" name="kode_inventaris" value="<?php echo $d['kode_inventaris']; ?>"></td>
    							</tr>
    							<tr>
    								<td>NAMA PETUGAS</td>
    								<td>
    									<select name="nama_petugas" class="form-control">
    										<option hidden>Pilih petugas</option>
    										<?php
    										$query=mysqli_query($koneksi,"SELECT * FROM petugas");
    										while ($show=mysqli_fetch_array($query)) {?>
    										<option value="<?=$show['id_petugas'];?>" <?=($d['id_petugas']==$show['id_petugas'])?'selected':''?>><?=$show['nama_petugas'];?>
    										</option>
    										<?php
    									}
    									?>
    								</select>
    							</td>
    						</tr>
    						<tr>
    							<td></td>
    							<td><input type="submit" value="SIMPAN"></td>
    						</tr>		
    					</table>
    				</form>
    				<?php 
    			}
    			?>
    			
    			<!-- /.row -->
    			<!-- /.row -->
    		</div>
    		<!-- /#page-wrapper -->
    	</div>
    </div>
</div>
</div>
<!-- /#wrapper -->

<!-- jQuery -->

<?php include 'scripts.php'; ?>
</body>

</html>

