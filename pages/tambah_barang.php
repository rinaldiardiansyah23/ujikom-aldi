<!DOCTYPE html>
<html lang="en">

<head>
<title>INSKAN</title>

    <?php include 'links.php'; ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <?php include 'header.php'; ?>
    <div id="wrapper">
 <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tambah barang</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                 <form action="simpan_barang.php" method="post">
                                     
                                        <div class="form-group">
                                            <label>NAMA</label>
                                            <input name="nama" class="form-control" placeholder="Masukan Nama Anda">
                                        </div>
                                        <div class="form-group">
                                            <label>KONDISI</label>
                                            <input name="kondisi" class="form-control" placeholder="Masukan kondisi barang">
                                        </div>
                                        <div class="form-group">
                                            <label>KETERANGAN</label>
                                            <input name="keterangan" class="form-control" placeholder="Masukan Ketarangan">
                                        </div>
                                        <div class="form-group">
                                            <label>JUMLAH</label>
                                            <input name="jumlah" class="form-control" type="number" min="1" autocomplete="off" placeholder="Masukan Jumlah Barang">
                                        </div>
                                        <div class="form-group">
                                            <label>JENIS</label>
                                            <select name="nama_jenis" class="form-control">
                                            <option hidden>Pilih jenis</option>
                                            <?php
                                                include "koneksi.php";
                                                $query=mysqli_query($koneksi,"SELECT * FROM jenis");
                                                while ($show=mysqli_fetch_array($query)) {
                                                    echo "<option value='$show[id_jenis]'>$show[nama_jenis]
                                                    </option>";
                                                }
                                            ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>TANGGAL</label>
                                            <input name="tanggal_register" type="date" class="form-control" placeholder="Masukan Tanggal">
                                        </div>
                                        <div class="form-group">
                                            <label>NAMA RUANG</label>
                                            <select name="nama_ruang" class="form-control">
                                            <option hidden>Pilih Ruang</option>
                                            <?php
                                                $query=mysqli_query($koneksi,"SELECT * FROM ruang");
                                                while ($show=mysqli_fetch_array($query)) {
                                                    echo "<option value='$show[id_ruang]'>$show[nama_ruang]
                                                    </option>";
                                                }
                                            ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>KODE</label>
                                            <input name="kode_inventaris" class="form-control" type="text" placeholder="Masukan Kode ">
                                        </div>
                                        <div class="form-group">
                                            <label>NAMA PETUGAS</label>
                                            <select name="nama_petugas" class="form-control">
                                            <option hidden>Pilih Petugas</option>
                                            <?php
                                                $query=mysqli_query($koneksi,"SELECT * FROM petugas");
                                                while ($show=mysqli_fetch_array($query)) {
                                                    echo "<option value='$show[id_petugas]'>$show[nama_petugas]
                                                    </option>";
                                                }
                                            ?>
                                            </select>
                                        </div>                                
										
    
                                        <button type="submit" class="btn btn-default">Simpan</button>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
</div>
    <?php include 'scripts.php'; ?>
</body>
</html>